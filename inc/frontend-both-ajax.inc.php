<?php
// Front Both Ajax
defined( 'ABSPATH' ) or	die( 'Cheatin\' uh?' );

/**
* LOAD LEAFLET
* @uses nbm_load_leaflet FUNCTION to register scripts and load styles
* @uses nbm_load_ie_styles FUNCTION to print script only for ie
* @uses nbm_lang_init FUNCTION to load language files
*/
add_action( 'wp_enqueue_scripts', 'nbm_load_leaflet' );
function nbm_load_leaflet() {
	wp_register_script( 'leaflet', NBM_PLUGIN_URL . '/leaflet/leaflet-src.js', false, '0.5.1', true );
	wp_register_script( 'leaflet-animated-marker', NBM_PLUGIN_URL . '/leaflet/AnimatedMarker.min.js', array('leaflet'), '0.5.1', true );
	wp_register_script( 'leaflet-script', NBM_PLUGIN_URL . '/js/leaflet-script.js', array('leaflet', 'leaflet-animated-marker', 'jquery' ), '1.4.999999', true );

	wp_register_style( 'leaflet', NBM_PLUGIN_URL . '/leaflet/leaflet.css', false, '0.5.1', 'all');
	wp_register_style( 'leaflet-lte-ie8', NBM_PLUGIN_URL . '/leaflet/leaflet.ie.css', array('leaflet'), '1.1', 'all');
	$GLOBALS['wp_styles']->add_data( 'leaflet-lte-ie8', 'conditional', 'lte IE 8' );
	wp_register_style( 'leaflet-maps-style', NBM_PLUGIN_URL . '/css/maps.css', array('leaflet'), '1.1', 'all');
}