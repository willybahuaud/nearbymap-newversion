<?php
// Always
defined( 'ABSPATH' ) or	die( 'Cheatin\' uh?' );

// Add language support
add_action( 'init', 'nbm_lang_init' );
function nbm_lang_init() {
	load_plugin_textdomain( 'nbm', false, basename( dirname( NBM__FILE__ ) ) . '/lang/' );
}

/**
* GET ROUTE
* @uses nbm_get_route FUNCTION to retrieve routes datas
*
* @uses cloudmade_key FILTER HOOK to change cloudmade key
* @uses nbm_dir_transient_life FILTER HOOK to change the life duration of the transient data
*/
add_action( 'wp_ajax_nopriv_nbm_get_route', 'nbm_get_route' );
add_action( 'wp_ajax_nbm_get_route', 'nbm_get_route' );
function nbm_get_route(){
	if( isset( $_POST['nbm-start'], $_POST['nbm-end'], $_POST['nbm-trans']) ){
		$start = $_POST['nbm-start'];
		$end   = $_POST['nbm-end'];
		$mode  = $_POST['nbm-trans'];

		if( $start == 'custom' )
			$start = implode( ',', nbm_get_coords( $_POST['nbm-address-start'] ) );
		if( $end == 'custom' )
			$end = implode( ',', nbm_get_coords( $_POST['nbm-address-end'] ) );


		// set URL and other appropriate options
		$key = apply_filters( 'cloudmate_key', '8EE2A50541944FB9BCEDDED5165F09D9' ); // deprecated since 0.9.4
		$key = apply_filters( 'cloudmade_key', '8EE2A50541944FB9BCEDDED5165F09D9' );
		$url = 'http://navigation.cloudmade.com/' . $key . '/api/latest/' . $start . ',' . $end . '/' . $mode . '.js?lang=' . substr( get_locale(), 0, 2 );
		if( !$response = get_transient( 'nbm_dir_' . md5( $url ) ) ){
			$response = wp_remote_get( $url );
			if( !is_wp_error( $response ) ){
				echo wp_remote_retrieve_body( $response );
				set_transient( 'nbm_dir_' . md5( $url ), $response, apply_filters( 'nbm_dir_transient_life', DAY_IN_SECONDS ) );
			}
		}else
			echo wp_remote_retrieve_body( $response );
		exit;
	}
}

// Add a custom taxo
add_action( 'init', 'nbm_register_taxo' );
function nbm_register_taxo(){
	if( apply_filters( 'nbm_catplace_taxo', 'catplace' ) == 'catplace' ) {

		$labels = array(
			'name'                       => _x( 'Place Categories', 'Taxonomy General Name', 'nbm' ),
			'singular_name'              => _x( 'Place Category', 'Taxonomy Singular Name', 'nbm' ),
			'menu_name'                  => __( 'Place Categories', 'nbm' ),
			'all_items'                  => __( 'All Place Categories', 'nbm' ),
			'parent_item'                => __( 'Parent Place Category', 'nbm' ),
			'parent_item_colon'          => __( 'Parent Place Category:', 'nbm' ),
			'new_item_name'              => __( 'New Place Category', 'nbm' ),
			'add_new_item'               => __( 'Add New Place Category', 'nbm' ),
			'edit_item'                  => __( 'Edit Place Category', 'nbm' ),
			'update_item'                => __( 'Update Place Category', 'nbm' ),
			'separate_items_with_commas' => __( 'Separate Place Categories with commas', 'nbm' ),
			'search_items'               => __( 'Search Place Categories', 'nbm' ),
			'add_or_remove_items'        => __( 'Add or remove Place Categories', 'nbm' ),
			'choose_from_most_used'      => __( 'Choose from the most used Place Categories', 'nbm' ),
		);

		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
		);

		if( apply_filters( 'nbm_post_type_capa_post', true ) )
				$args['capabilities'] = array(
				'manage_terms'               => 'manage_categories',
				'edit_terms'                 => 'manage_categories',
				'delete_terms'               => 'manage_categories',
				'assign_terms'               => 'edit_posts',
			);
		else
				$args['capabilities'] = array(
				'manage_terms'               => 'manage_catplaces',
				'edit_terms'                 => 'manage_catplaces',
				'delete_terms'               => 'manage_catplaces',
				'assign_terms'               => 'edit_places',
			);

		$args = apply_filters( 'nbm_catplace_args', $args );
		register_taxonomy( 'catplace', apply_filters( 'nbm_post_type', 'places' ), $args );
	}
}

/**
* RETRIEVE DATA
* @uses nbm_get_coords FUNCTION to retrieve coords
* @uses get_distance FUNCTION to calculate distance betwenn places
*
* @uses nbm_try_to_find_with_openstreetmap FILTER HOOK to specify if you don't want to retrieve coordinates with openstreetmap (increase precision, but it's google...)
*/
function nbm_get_coords( $address ) {
	$map_url = 'http://nominatim.openstreetmap.org/search?format=json&q=' . urlencode( $address );
	$request = wp_remote_get( $map_url );
	$json    = wp_remote_retrieve_body( $request );

	$tryosm = apply_filters( 'nbm_try_to_find_with_openstreetmap', true );

	if( $json != '[]' && $tryosm ) {
		$json = json_decode( $json );

		$long = $json[0]->lon;
		$lat  = $json[0]->lat;

		return compact( 'lat', 'long' );
	} else { // f**k, there are no results with openstreetmap... let's try on google maps
		$map_url = 'http://maps.google.com/maps/api/geocode/json?sensor=false&address=' . urlencode( $address );
		$request = wp_remote_get( $map_url );
		if( is_wp_error( $request ) )
			return false;

		$json = wp_remote_retrieve_body( $request );

		if( empty( $json ) )
			return false;

		$json = json_decode($json);

		$status = $json->status;

		if ( $status == 'OK' ){
			$lat = $json->results[0]->geometry->location->lat;
			$long = $json->results[0]->geometry->location->lng;

			return compact( 'lat', 'long' );
		}else{
			return false;
		}
	}
}

add_filter( 'cloudmade_key', 'nbm_option_filter_cloudmade_key', 1 );
function nbm_option_filter_cloudmade_key( $key )
{
	$nbm_options = get_option( 'nbm' );
	return !empty( $nbm_options['apikey'] ) ? esc_attr( $nbm_options['apikey'] ) : $key;
}

add_filter( 'cloudmade_style', 'nbm_option_filter_cloudmade_style', 1 );
function nbm_option_filter_cloudmade_style( $style )
{
	$nbm_options = get_option( 'nbm' );
	return !empty( $nbm_options['style'] ) ? (int)$nbm_options['style'] : $style;
}

add_filter( 'nbm_external_user_google', 'nbm_option_filter_nbm_external_user_google', 1 );
function nbm_option_filter_nbm_external_user_google()
{
	$nbm_options = get_option( 'nbm' );
	return true;
	// return isset( $nbm_options['service'] ) && $nbm_options['service']=='google';
}

add_filter( 'nbm_need_route', 'patch_version_3_route', PHP_INT_MAX );
function patch_version_3_route() {
	return false;
}

add_filter( 'maps_datas', 'patch_version_3_maps_datas', PHP_INT_MAX );
function patch_version_3_maps_datas( $data ) {
	$data['tiles'] = 'http://{s}.tile.osm.org/{z}/{x}/{y}.png';
	$data['attribution'] = '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors';
	// $data['subdomains'] = 'abc';
	return $data;
}