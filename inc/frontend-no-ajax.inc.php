<?php
// Front No Ajax
defined( 'ABSPATH' ) or	die( 'Cheatin\' uh?' );

/**
* SHORTCODE
* @uses nbm_render_map FUNCTION to render the general map
* @uses nbm_place_information FUNCTION to render place information
*
* @uses [maps] SHORTCODE for the general map
* @uses [place] SHORTCODE for map information
*
* @uses nbm_need_more FILTER HOOK to disallow the list of places
* @uses nbm_need_route FILTER HOOK to disallow route system
* @uses nbm_places_link FILTER HOOK to disallow links to single place
* @uses nbm_map_height FILTER HOOK to force a map's height
* @uses nbm_map_width FILTER HOOK to force a map's width
* @uses markers_querys FILTER HOOK to overide marker query
* @uses nbm_post_type FILTER HOOK to targeting the right post type (instead of place)
* @uses nbm_icon FILTER HOOK to alter color and marker's letter
* @uses nbm_map FILTER HOOK to alter general map HTML
*/

//GENERAL MAP
add_shortcode( 'maps', 'nbm_render_map' );
add_shortcode( 'map', 'nbm_render_map' );
function nbm_render_map( $atts ){
	//LOAD SCRIPTS
	wp_enqueue_script( 'leaflet' );
	wp_enqueue_script( 'leaflet-animated-marker' );
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'leaflet-script' );
	wp_enqueue_style( 'leaflet' );
	wp_enqueue_style( 'leaflet-maps-style' );
	wp_enqueue_style( 'leaflet-lte-ie8' );
	$mapid = isset( $atts['mapid'] ) ? sanitize_key( $atts['mapid'] ) : sanitize_key( uniqid( rand(0,10000), true ) );
	// Usage of filters to override the shortcodes params like a brute
	$filter_nbm_need_more = isset( $atts['nbm_need_more'] ) ? (bool)$atts['nbm_need_more'] : apply_filters( 'nbm_need_more', true );
	$filter_nbm_need_route = isset( $atts['nbm_need_route'] ) ? (bool)$atts['nbm_need_route'] : apply_filters( 'nbm_need_route', true );
	$filter_nbm_places_link = isset( $atts['nbm_places_link'] ) ? (bool)$atts['nbm_places_link'] : apply_filters( 'nbm_places_link', true );
	$map_height = isset( $atts['height'] ) ? 'height:'.(int)$atts['height'].'px;' : apply_filters( 'nbm_map_height', '' );
	$map_width = isset( $atts['width'] ) ? 'width:'.(int)$atts['width'].'px;' : apply_filters( 'nbm_map_width', '' );
	$force_icon = isset( $atts['icon'] ) ? $atts['icon'] : false;
	if( $GLOBALS['post'] && isset( $atts['post__in'] ) && $atts['post__in']=='this' )
		$atts['post__in'] = array( $GLOBALS['post']->ID );
	$args = $atts;
	if( $args )
		unset( $args['nbm_places_link'], $args['nbm_need_route'], $args['nbm_need_more'], $args['height'], $args['width'], $args['icon'] ); // Do not use this into query
	$args = wp_parse_args( $args, apply_filters( 'markers_querys', array(
			'posts_per_page' => -1,
			'orderby'        => 'meta_value_num',
			'order'        	 => 'ASC',
			'meta_key'       => '_nbm_important',
			), $args ) );
	$args['post_type'] = apply_filters( 'nbm_post_type', 'places' ); // Force usage of hookable post type
	$args = array_map( 'maybe_unserialize', $args );
	//INITIALIZE MAP DATAS
	//// wp_die(var_dump($args));
	$places = new WP_Query( $args );

	//INITIALIZE AN OUTPUT
	$output = '<div id="' . $mapid . '" class="nbm-map" style="'.$map_width.$map_height.'"></div>';
	nbm_deep_into_my_script( $mapid, $places, $atts );

	if( $filter_nbm_need_more ) {

		$letters   = nbm_return_icon_data( 'letters' );
		$important = get_option( 'nbm_important' );
		$ref       = get_post_meta( $important, '_nbm_coords', true );

		$points	   = array();

		$output .= '<div class="nbm-all-places" data-map="' . $mapid . '">';

		$last_letter = 'A';

		if( $places->have_posts() ) : while( $places->have_posts() ) : $places->the_post();

			$type = get_post_meta( $places->post->ID, '_nbm_icon_type', true );
			$type = $type ? $type : 'icon';
			$type = apply_filters( 'nbm_icon_type', $type, $post );
			//datas
			if( $force_icon ){
				$icons = explode( ',', $atts['icon'] );
				$icons = array_map( 'sanitize_key', $icons );
				$icon = array( 'color'=>reset($icons), 'letter'=>end($icons) );
				$icon = apply_filters( 'nbm_icon', $icon, $places->post );
			}else{
				$icon = apply_filters( 'nbm_icon', get_post_meta( $places->post->ID, '_nbm_icon', true ), $places->post );
			}
			if( isset( $letters[ $icon[ 'letter'] ] ) ) // back compat, deprecated since 0.9.4
				$icon[ 'letter'] = $letters[ $icon[ 'letter'] ];
			if( $type=='letter' ){
				$icon[ 'letter'] = $last_letter;
				$last_letter++;
			}else{
				if( isset( $letters[ $icon[ 'letter'] ] ) ) //  back compat, deprecated since 0.9.4
					$icon[ 'letter'] = $letters[ $icon[ 'letter'] ];
			}
			$type_class = $type=='letter' ? ' nbm-letter' : '';
			$img = ( $img = get_the_post_thumbnail( $places->post->ID, 'nbm-size', array( 'class' => 'nbm-info-pict'.$type_class ) ) ) ? preg_replace('/<(.*)>/', '<$1 itemprop="image">', $img ) : '<div class="nbm-pict nbm-info-pict ' . $icon[ 'color'] . $type_class .'">' . $icon['letter'] . '</div>' ;
			$coords = get_post_meta( $places->post->ID, '_nbm_coords', true );

			$output .= '<div data-nbm="'.$places->post->ID.'" class="nbm-more ' . $icon[ 'color'] . '" itemscope itemtype="http://schema.org/' . ( ( $type_of_place = get_post_meta( $places->post->ID, '_nbm_type_of_place', true ) ) ? sanitize_html_class( $type_of_place ) : 'Place' ) . '">';
			$output .= $img;
			$output .= ( $important != false && $important != $places->post->ID ) ? '<div class="nbm-distance">' . get_distance( $ref['lat'], $ref['long'], $coords['lat'], $coords['long'] ) . 'km</div>' : '';
			$output .= the_title( '<strong class="nbm-info-title" itemprop="name">', '</strong>', false );
			$output .= '<div class="nbm-infos-comp">';
				$output .= ( $tel = get_post_meta( $places->post->ID, '_nbm_tel', true ) ) ? '<div class="nbm-info-comp"><a href="tel:'.$tel.'" title="'.$tel.'" itemprop="telephone">'.$tel.'</a></div>' : '';
				$output .= ( $email = get_post_meta( $places->post->ID, '_nbm_email', true ) ) ? '<div class="nbm-info-comp"><a href="mailto:'.antispambot($email).'" title="'.antispambot($email).'">'.antispambot($email).'</a></div>' : '';
				$output .= ( $website = get_post_meta( $places->post->ID, '_nbm_website', true ) ) ? '<div class="nbm-info-comp"><a href="'.esc_url($website).'" target="_blank" title="'.esc_url($website).'">'.esc_url($website).'</a></div>' : '';
				$output .= '<div itemprop="geo" itemscope itemtype="http://schema.org/GeoCoordinates">';
					$output .= '<meta itemprop="latitude" content="' . $coords[ 'lat'] . '" />';
	    			$output .= '<meta itemprop="longitude" content="' . $coords[ 'long'] . '" />';
    			$output .= '</div>';
			$output .= '</div>';
			if( $filter_nbm_places_link )
				$output .= '<a class="nbm-more-link" href="' . get_permalink() . '" itemprop="url">' . __( 'More information', 'nbm' ) . '</a>';
			$output .= '</div>';

			$points[the_title('','',false)] = $coords;
		endwhile; endif;
		$output .= '</div>';
	}

	if( $filter_nbm_need_route ) {
		if( ! isset( $points ) ) {
			$points	= array();
			if( $places->have_posts() ) : while( $places->have_posts() ) : $places->the_post();
				$points[the_title('','',false)] = get_post_meta( $places->post->ID, '_nbm_coords', true );
			endwhile; endif;
		}
		array_filter( $points );

		// RENDER FORM
		$output .= '<div class="nbm-route" data-map="' . $mapid . '"><form class="nbm-route-form" data-map="' . $mapid . '">';

			//options

			//from
			$sel = '<div class="nbm-route-selects"><div class="nbm-route-from nbm-route-point">';
			$sel .= '<select name="nbm-start" class="nbm-select">';
			$sel .= '<option value="">' . __( 'Starting point', 'nbm' ) . '</option>';
			foreach( $points as $k => $p )
				$sel .= '<option value="' . esc_attr( $p[ 'lat'] . ',' . $p[ 'long'] ) . '">' . esc_html( $k ) . '</option>';
			$sel .= '<option value="custom">' . __( 'Custom start', 'nbm' ) . '</option>';
			$sel .= '</select>';
			$sel .= '<input type="text" placeholder="' . __( 'address', 'nbm' ) . '" name="nbm-address-start" class="nbm-route-custom-point hidden">';
			$sel .= '</div><div class="nbm-sep">➜</div>';

			$output .= $sel;

			//to
			$sel2 = '<div class="nbm-route-to nbm-route-point">';
			$sel2 .= '<select name="nbm-end" class="nbm-select">';
			$sel2 .= '<option value="">' . __( 'Ending point', 'nbm' ) . '</option>';
			foreach( $points as $k => $p )
				$sel2 .= '<option value="' . esc_attr( $p[ 'lat'] . ',' . $p[ 'long'] ) . '">' . esc_html( $k ) . '</option>';
			$sel2 .= '<option value="custom">' . __( 'Custom destination', 'nbm' ) . '</option>';
			$sel2 .= '</select>';
			$sel2 .= '<input type="text" placeholder="' . __( 'address', 'nbm' ) . '" name="nbm-address-end" class="nbm-route-custom-point hidden">';
			$sel2 .= '</div></div>';

			$output .= $sel2;

			//options
			$output .= '<div class="nbm-reset-before-options"></div><a href="javascript:void(0)" class="more-options" data-map="' . $mapid . '">' . __( 'More options', 'nbm' ) . '</a>';
			$output .= '<div class="nbm-route-options" style="display:none" data-map="' . $mapid . '">';
			$output .= '<label><input type="radio" name="nbm-trans" class="nbm-trans" value="car" checked="checked"> ' . __( 'Car (fastest)', 'nbm' ) . '</label><br/>';
			$output .= '<label><input type="radio" name="nbm-trans" class="nbm-trans" value="car/shortest"> ' . __( 'Car (shortest)', 'nbm' ) . '</label><br/>';
			$output .= '<label><input type="radio" name="nbm-trans" class="nbm-trans" value="bicycle"> ' . __( 'Bicycle', 'nbm' ) . '</label><br/>';
			$output .= '<label><input type="radio" name="nbm-trans" class="nbm-trans" value="foot"> ' . __( 'Foot', 'nbm' ) . '</label>';
			$output .= '</div>';

			//submit
			$output .= '<button type="submit" class="nbm-route-submit">'.__( 'Display/Hide route', 'nbm' ) . ' <img src="'.admin_url('images/wpspin_light.gif').'" height="12" width="12" style="display:none" class="nbm_load" alt="Ajax Loading"/></button>';


		$output .= '<div class="route-details" data-map="' . $mapid . '"></div>';
		$output .= '</form>';
		$output .= '</div>';
		$output .= '<noscript>';
		$output .= '<style>.nbm-map,.nbm-all-places,.nbm-route{display:none;}</style>';
		$output .= '<p>';
		$output .= __( 'NearbyMap needs JavaScript activated.', 'nbm' );
		$output .= '<img src="' . NBM_PLUGIN_URL . '/css/img/map_placeholder.jpg" alt="Map Placeholder" />';
		$output .= '</p></noscript>';


		wp_localize_script( 'leaflet-script', 'routeDial', array(
			'to' => _x( 'to', 'nbm', 'nbm' ),
			'in' => _x( 'in', 'nbm', 'nbm' ),
			'and' => _x( 'and', 'nbm', 'nbm' ),
			'minutes' => _x( 'mn', 'nbm', 'nbm' ),
			'hours' => _x( 'H', 'nbm', 'nbm' ),
			'days' => _x( 'd', 'nbm', 'nbm' ),
			'transIcon' => array(
				'car' => NBM_PLUGIN_URL . '/css/img/pins/car.png',
				'bicycle' => NBM_PLUGIN_URL . '/css/img/pins/bicycle.png',
				'foot' => NBM_PLUGIN_URL . '/css/img/pins/foot.png'
				)
			) );
	}
	wp_reset_postdata();
	return apply_filters( 'nbm_map', $output, $atts );
}

//EACH ITEM
// @uses nbm_place_information FILTER HOOK to alter returned information for a single place
// @uses nbm_data_place FILTER HOOK to alter displayed data on [place] shortcode
add_shortcode( 'place', '___nbm_place_information' );
function ___nbm_place_information( $atts )
{
	if( ! $atts )
		$atts = array();
	$atts['post__in'] = 'this';
	$atts['nbm_need_more'] = '0';
	return nbm_render_map( $atts );
}


/**
* LOAD DATAS INTO LEAFLET
* @uses nbm_deep_into_my_script FUNCTION to load datas vars (for leaflet)
*
* @uses markers_querys FILTER HOOK to overide marker query
* @uses nbm_post_type FILTER HOOK to targeting the right post type (instead of place)
* @uses cloudmade_key FILTER HOOK to change cloudmade key
* @uses maps_datas FILTER HOOK to modify send datas
* @uses nbm_icon FILTER HOOK to alter color and marker's letter
* @uses nbm_size FILTER HOOK to alter marker's size
* @uses nbm_data_marker FILTER HOOK to alter displayed data on [maps] shortcode
* @uses cloudmade_key FILTER HOOK to alter cloudmade licence key
* @uses cloudmade_style FILTER HOOK to alter cloudmade map' style
*/
function nbm_deep_into_my_script( $mapid, $queryobject, $atts ){

	$markers = array();
	$icons 	 = array();
	$places = $queryobject;

	$important = get_option( 'nbm_important' );
	$last_letter = 'A';
	if( $places->have_posts() ) :
		while( $places->have_posts() ) : $places->the_post();
			$force_icon = isset( $atts['icon'] ) ? $atts['icon'] : false;
			$coords = get_post_meta( $places->post->ID, '_nbm_coords', true );
			if( $force_icon ){
				$icons = explode( ',', $atts['icon'] );
				$icons = array_map( 'sanitize_key', $icons );
				$icon = array( 'color'=>reset($icons), 'letter'=>end($icons) );
				$icon = apply_filters( 'nbm_icon', $icon, $places->post );
			}else{
				$icon = apply_filters( 'nbm_icon', get_post_meta( $places->post->ID, '_nbm_icon', true ), $places->post );
			}
			$type = isset( $atts['type'] ) ? $atts['type'] : get_post_meta( $places->post->ID, '_nbm_icon_type', true );
			$type = apply_filters( 'nbm_icon_type', $type, $places->post );
			$letters = nbm_return_icon_data( 'letters' );
			if( $type=='letter' ){
				$icon[ 'letter'] = $last_letter;
				$last_letter++;
			}else{
				if( isset( $letters[ $icon[ 'letter'] ] ) ) //  back compat, deprecated since 0.9.4
					$icon[ 'letter'] = $letters[ $icon[ 'letter'] ];
			}
			$size = apply_filters( 'nbm_size', get_post_meta( $places->post->ID, '_nbm_size', true ), $places->post );

			$datas = array();
			if( $tel = get_post_meta( $places->post->ID, '_nbm_tel', true ) ) $datas[] = '<a href="tel:' . $tel . '" title="' . $tel . '">g</a>';
			if( $email = get_post_meta( $places->post->ID, '_nbm_email', true ) ) $datas[] = '<a href="mailto:' . antispambot( $email ) . '" title="' . antispambot( $email ) . '">X</a>';
			if( $website = get_post_meta( $places->post->ID, '_nbm_website', true ) ) $datas[] = '<a href="' . esc_url( $website ) . '" target="_blank" title="' . esc_attr( esc_url( $website ) ) . '">^</a>';
			if( !apply_filters( 'nbm_external_user_google', false ) )
				$external_url = 'http://geocoding.cloudmade.com/'.apply_filters( 'cloudmade_key', '8EE2A50541944FB9BCEDDED5165F09D9' ).'/geocoding/v2/find.html?query=%s';
			else
				$external_url = 'https://maps.google.com/maps?q=%s';
			$address = ( str_replace( array( "\n", ' ' ), '+', get_post_meta( $places->post->ID, '_nbm_address', true ) ) );
			$external_url = apply_filters( 'nbm_external_url', $external_url, $address, $coords );
			$external_url = sprintf( $external_url, $address );
			$datas[] = '<a href="' . esc_url( $external_url ) . '" target="_blank" title="' . esc_attr( esc_url( $external_url ) ) . '">Ç</a>';
			$datas = join( '|', $datas );
			if( $tooltip = get_post_meta( $places->post->ID, '_nbm_tooltip', true ) ){
				$tooltip = '<p>' . $tooltip . '</p>';
			}else{
				$tooltip = '<br />';
			}
			$datas = sprintf( '<b class="popup-title">%s</b>%s<div class="marker-datas">%s</div>', apply_filters( 'the_title', $places->post->post_title ), wp_kses_post( str_replace( "\n", '<br />', $tooltip ) ), $datas );
			$datas = apply_filters( 'nbm_data_marker', $datas, $places->post );
			$markers[] = array(
				'id'     => $places->post->ID,
				'icon'   => $icon,
				'type'	 => sanitize_html_class( $type ),
				'size'   => $size,
				'import' => $important == $places->post->ID ? true : false,
				'coords' => $coords,
				'datas'  => $datas
				);
		endwhile;
	endif;

	$key = apply_filters( 'cloudmade_key', '4D7C045AF92D4BCA9199E50BD83B1A46' );
	$style = isset( $atts['style'] ) ? sanitize_key( $atts['style'] ) : '997';
	$style = apply_filters( 'cloudmade_style', $style, $mapid );
	$maps_datas = array(
		'tiles'       => 'http://{s}.tile.cloudmade.com/' . $key . '/' . $style . '/256/{z}/{x}/{y}.png',
		'attribution' => ' &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> / <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a> / © <a href="http://cloudmade.com">CloudMade</a>',
		'subdomains'  => 'abc',
		'admin_url'   => admin_url( 'admin-ajax.php' ),
		'markers'     => $markers,
		'max-zoom'    => apply_filters( 'nbm_max_zoom', 18, count( $markers ), $mapid ),
		'iconProp'    => array(
			'iconSizeSmall'     => array( 32, 44 ),
			'iconAnchorSmall'   => array( 16, 44 ),
			'popupAnchorSmall'  => array( 0, -44 ),
			'iconSizePin'       => array( 32, 44 ),
			'iconAnchorPin'     => array( 16, 44 ),
			'popupAnchorPin'    => array( 0, -44 ),
			'iconSizeMedium'    => array( 48, 61 ),
			'iconAnchorMedium'  => array( 24, 61 ),
			'popupAnchorMedium' => array( 0, -61 ),
			'iconSizeLarge'     => array( 55, 71 ),
			'iconAnchorLarge'   => array( 27, 71 ),
			'popupAnchorLarge'  => array( 0, -71 ),
			'className'         => 'icon',
			)
		);
	wp_localize_script( 'leaflet-script', 'maps_datas_' . $mapid, apply_filters( 'maps_datas', $maps_datas ) );
}

function get_distance( $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000 ) {
	// convert from degrees to radians
	$latFrom = deg2rad( $latitudeFrom );
	$lonFrom = deg2rad( $longitudeFrom );
	$latTo   = deg2rad( $latitudeTo );
	$lonTo   = deg2rad( $longitudeTo );

	$lonDelta = $lonTo - $lonFrom;
	$a = pow( cos( $latTo ) * sin( $lonDelta ), 2) + pow( cos( $latFrom ) * sin( $latTo ) - sin( $latFrom ) * cos( $latTo ) * cos( $lonDelta ), 2);
	$b = sin( $latFrom ) * sin( $latTo ) + cos( $latFrom ) * cos( $latTo ) * cos( $lonDelta );

	$angle = atan2( sqrt( $a ), $b );
	return intVal( $angle * $earthRadius / 1000 );
}