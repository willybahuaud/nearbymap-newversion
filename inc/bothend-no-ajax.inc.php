<?php
// Both no ajax
defined( 'ABSPATH' ) or	die( 'Cheatin\' uh?' );

// ADD A SPECIFIC IMAGE SIZE
add_action( 'init', 'nbm_register_size' );
function nbm_register_size(){
	add_image_size( 'nbm-size', 100, 100, true );
}

/**
* CREATE PLACES
* @uses nbm_register_places FUNCTION to register the CPT
*
* @uses nbm_post_type FILTER HOOK to target an existing post type instead creating one
* @uses places_args FILTER HOOK to modify places property
*/
add_action( 'init', 'nbm_register_places' );
function nbm_register_places() {
	if( apply_filters( 'nbm_post_type', 'places' ) == 'places' ) {
		$places_args = array(
			'label' => __( 'Places', 'nbm' ),
			'labels' => array(
				'name'          => __( 'Places', 'nbm' ),
				'singular_name' => __( 'Place', 'nbm' ),
				'all_items'		=> __( 'All places', 'nbm' ),
				'add_new'       => __( 'Add place', 'nbm' ),
				'add_new_item'  => __( 'Add a new place', 'nbm' ),
				'edit_item'     => __( 'Edit place', 'nbm' ),
				'new_item'      => __( 'Add a place', 'nbm' ),
				'view_item'     => __( 'View place', 'nbm' )
				),
			'public' 	=> true,
			'supports' 	=> array( 'title', 'editor', 'thumbnail' )
			);

		if( apply_filters( 'nbm_post_type_capa_post', true ) )
			$places_args['capability'] = 'post';
		else
			$places_args['capabilities'] = array(
											'edit_post'          => 'edit_place',
											'read_post'          => 'read_place',
											'delete_post'        => 'delete_place',
											'edit_posts'         => 'edit_places',
											'edit_others_posts'  => 'edit_others_places',
											'publish_posts'      => 'publish_places',
											'read_private_posts' => 'read_private_places',
										);

		$places_args = apply_filters( 'places_args', $places_args ); // deprecated since 0.9.4
		$places_args = apply_filters( 'nbm_places_args', $places_args );
		register_post_type( 'places', $places_args );
	}
}

/**
* ICONS
* @uses nbm_return_icon_data FUNCTION to generate vars with all icons color|pictos
*/
function nbm_return_icon_data( $what ) {
	if( $what == 'colors' ) {
		return array(
			'blue'        => '#8bc6dd',
			'navy'        => '#247ba1',
			'brown'       => '#b27349',
			'lightbrown'  => '#da804b',
			'green'       => '#87a85f',
			'darkgreen'   => '#4a6926',
			'grey'        => '#485a61',
			'lightgrey'   => '#bfbfbf',
			'orange'      => '#e09749',
			'lightorange' => '#d45e00',
			'pink'        => '#dc5888',
			'lightpink'   => '#f978a1',
			'purple'      => '#6e5da5',
			'lavande'     => '#7b67cb',
			'red'         => '#c15656',
			'fire'        => '#ef1616',
			'yellow'      => '#dab049',
			'realyellow'  => '#f6ab19',
			);
	}
	if( $what == 'letters' ) { // DEPRECATED SINCE 0.9.4 DO NOT USE, DO NOT DELETE YET
		return array(
			'camera-alt'          => 'b',
			'basket'              => 'c',
			'aboveground-rail'    => 'h',
			'airfield'            => 'i',
			'airport'             => 'j',
			'art-gallery'         => 'k',
			'bar'                 => 'l',
			'baseball'            => 'm',
			'basketball'          => 'n',
			'beer'                => 'o',
			'belowground-rail'    => 'p',
			'bicycle'             => 'q',
			'bus'                 => 'r',
			'cafe'                => 's',
			'campsite'            => 't',
			'cemetery'            => 'u',
			'cinema'              => 'v',
			'college'             => 'w',
			'commerical-building' => 'x',
			'credit-card'         => 'y',
			'cricket'             => 'z',
			'embassy'             => 'A',
			'fast-food'           => 'B',
			'ferry'               => 'C',
			'fire-station'        => 'D',
			'football'            => 'E',
			'fuel'                => 'F',
			'garden'              => 'G',
			'giraffe'             => 'H',
			'golf'                => 'I',
			'grocery-store'       => 'J',
			'harbor'              => 'K',
			'heliport'            => 'L',
			'hospital'            => 'M',
			'industrial-building' => 'N',
			'library'             => 'O',
			'lodging'             => 'P',
			'london-underground'  => 'Q',
			'minefield'           => 'R',
			'monument'            => 'S',
			'museum'              => 'T',
			'pharmacy'            => 'U',
			'pitch'               => 'V',
			'police'              => 'W',
			'post'                => 'X',
			'prison'              => 'Y',
			'rail'                => 'Z',
			'religious-christian' => '0',
			'religious-islam'     => '1',
			'religious-jewish'    => '2',
			'restaurant'          => '3',
			'roadblock'           => '4',
			'school'              => '5',
			'shop'                => '6',
			'skiing'              => '7',
			'soccer'              => '8',
			'swimming'            => '9',
			'tennis'              => '&',
			'theatre'             => 'é',
			'toilet'              => '"',
			'town-hall'           => '\'',
			'trash'               => '(',
			'tree-1'              => '-',
			'tree-2'              => 'è',
			'warehouse'           => '_',
			'beaker'              => 'd',
			'stethoscope'         => 'f',
			'coffee'              => 'e',
			'phone'               => 'g',
			'link'                => 'a'
			);
	}
}