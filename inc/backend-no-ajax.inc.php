<?php
// Admin no ajax
defined( 'ABSPATH' ) or	die( 'Cheatin\' uh?' );

// Add admin CSS style
add_action( 'admin_enqueue_scripts', 'nbm_load_admin_style' );
function nbm_load_admin_style() {
	wp_register_style( 'nbm-admin-ui-css', NBM_PLUGIN_URL . '/css/nbm-admin-style.css', false, NBM_VERSION, false );
	wp_register_style( 'leaflet', NBM_PLUGIN_URL . '/leaflet/leaflet.css', false, '0.5.1', 'all');
	wp_register_style( 'leaflet-lte-ie8', NBM_PLUGIN_URL . '/leaflet/leaflet.ie.css', array('leaflet'), '1.1', 'all');
	$GLOBALS['wp_styles']->add_data( 'leaflet-lte-ie8', 'conditional', 'lte IE 8' );
	wp_register_style( 'nbm-options', NBM_PLUGIN_URL . '/css/nbm-options.css', false, NBM_VERSION, false );

	wp_register_script( 'leaflet', NBM_PLUGIN_URL . '/leaflet/leaflet-src.js', false, '0.5.1', true );
	wp_register_script( 'find-coords', NBM_PLUGIN_URL . '/js/find-coords.js', array( 'leaflet','jquery' ), '0.5.1', true );
}

/**
* CREATE METABOXES
* @uses nbm_create_metaboxes FUNCTION to register metaboxes
*
* @uses nbm_post_type FILTER HOOK to target an existing post type instead default
*/
add_action( 'admin_init', 'nbm_create_metaboxes' );
function nbm_create_metaboxes() {
	add_meta_box( 'nbm_infos', __( 'Information' , 'nbm' ), 'nbm_infos', apply_filters( 'nbm_post_type', 'places' ), 'side', 'default' );
	add_meta_box( 'nbm_loca_places', __( 'Location' , 'nbm' ), 'nbm_loca_places', apply_filters ('nbm_post_type', 'places' ), 'side', 'default' );
	add_meta_box( 'nbm_loc_icon', __( 'Pictos' , 'nbm' ), 'nbm_loc_icon', apply_filters( 'nbm_post_type', 'places' ), 'normal', 'default' );
}

/**
*SAVES METABOXES
* @uses nbm_save_metaboxes FUNCTION to save metaboxes
*/
add_action( 'save_post', 'nbm_save_metaboxes' );
function nbm_save_metaboxes( $post_ID ) {
	if( isset( $_POST['nbm_address'], $_POST['post_ID'] ) ) {
		check_admin_referer( 'nbm_coords-save_' . $_POST['post_ID'], 'nbm_coords-nonce' );

		$address = $_POST['nbm_address'];
		update_post_meta( $post_ID, '_nbm_address', $address );

		// manual coords ?
		if( isset( $_POST['nbm_do_u_define_coords'] ) ) {
			update_post_meta( $post_ID, '_nbm_do_u_define_coords', 1 );

			// sexagesimales ?
			if( !empty( $_POST['w_degres'] ) && !empty( $_POST['w_minutes'] ) && !empty( $_POST['w_secondes'] ) &&
				!empty( $_POST['n_degres'] ) && !empty( $_POST['n_minutes'] ) && !empty( $_POST['n_secondes'] ) ) {

				// CACULATE LONG / DEGRES + or - ?
				if( (int)$_POST['w_degres'] < 0 ) {
					$longitude = -1*( -1*( (int)( $_POST['w_degres'] ) ) + ( (int)( $_POST['w_minutes'] )/60 ) + ( (float)( $_POST['w_secondes'] )/3600 ) );
				} else {
					$longitude = -1*( (int)( $_POST['w_degres'] ) + ( (int)( $_POST['w_minutes'] )/60 ) + ( (float)( $_POST['w_secondes'] )/3600 ) );
				}

				// CALCULATE LAT
				$latitude = ( (int)( $_POST['n_degres'] ) ) + ( (int)( $_POST['n_minutes'] )/60 ) + ( (float)( $_POST['n_secondes'] )/3600 );

				$coords = array(
					'lat'  => $latitude,
					'long' => $longitude
					);
				update_post_meta( $post_ID, '_nbm_coords', $coords );

				$coords_sexa = array(
					'w_degres'   => (int)$_POST['w_degres'],
					'w_minutes'  => (int)$_POST['w_minutes'],
					'w_secondes' => (int)$_POST['w_secondes'],
					'n_degres'   => (int)$_POST['n_degres'],
					'n_minutes'  => (int)$_POST['n_minutes'],
					'n_secondes' => (int)$_POST['n_secondes']
					);
				update_post_meta( $post_ID, '_nbm_coords_sexa', $coords_sexa );

			// decimales ?
			} else {
				$user_coords = explode( ',', trim( $_POST['nbm_coords'] ) );

				//empty coords ?
				if( !empty( $user_coords[0] ) || !empty( $user_coords[1] ) )
					$coords = nbm_get_coords( $address );
				else
					$coords = array(
						'lat'  => (float)$user_coords[0],
						'long' => (float)$user_coords[1]
						);
				update_post_meta( $post_ID, '_nbm_coords', $coords );
			}
		// auto coords
		} else {
			update_post_meta( $post_ID, '_nbm_do_u_define_coords', 0 );
			$coords = nbm_get_coords( $address );

			// COORDS RETURN A RESULT
			if( !empty( $coords ) )
				update_post_meta( $post_ID, '_nbm_coords', $coords );
		}
	}
	if( isset( $_POST['nbm_icon-nonce'] ) ) {
		check_admin_referer( 'nbm_icon-save_' . $_POST['post_ID'], 'nbm_icon-nonce' );
		update_post_meta( $post_ID, '_nbm_icon', array(
			'color'  => $_POST['nbm-colors'],
			'letter' => $_POST['nbm-letters']
			) );
		$size = in_array( $_POST['nbm-size'], array( 'large', 'medium', 'small', 'pin' ) ) ? $_POST['nbm-size'] : 'medium';
		update_post_meta( $post_ID, '_nbm_size', $size );
		$type = $_POST['nbm-icon-type']=='letter' ? 'letter' : 'icon';
		update_post_meta( $post_ID, '_nbm_icon_type', $type );
	}

	if( isset( $_POST['nbm_infos-nonce'] ) ) {
		check_admin_referer( 'nbm_infos-save_' . $_POST['post_ID'], 'nbm_infos-nonce' );
		update_post_meta( $post_ID, '_nbm_tel', $_POST['nbm_tel'] );
		update_post_meta( $post_ID, '_nbm_email', is_email( $_POST['nbm_email'] ) ? $_POST['nbm_email'] : '' );
		update_post_meta( $post_ID, '_nbm_website', esc_url( $_POST['nbm_website'] ) );
		update_post_meta( $post_ID, '_nbm_tooltip', $_POST['nbm_tooltip'] );
		update_post_meta( $post_ID, '_nbm_hours', $_POST['nbm_hours'] );
		update_post_meta( $post_ID, '_nbm_type_of_place', sanitize_html_class( $_POST['nbm_type_of_place'] ) );
		$prev = get_option( 'nbm_important' );
		if( $_POST['nbm_important'] == 'yes' ) {
			update_post_meta( $prev, '_nbm_important', 2);
			update_option( 'nbm_important', $_POST['post_ID'] );
			update_post_meta( $post_ID, '_nbm_important', 1);
		} elseif( $prev == $post_ID ) {
			update_option( 'nbm_important', false );
			update_post_meta( $post_ID, '_nbm_important', 2);
		} else {
			update_post_meta( $post_ID, '_nbm_important', 2);
		}
	}

}


/**
* FUNCTIONS FOR METABOXES
* @uses nbm_loca_places FUNCTION to enter infos about location
* @uses nbm_loc_icon FUNCTION to chose a pictogram
* @uses nbm_infos FUNCTION to center general information
*/
function nbm_loca_places( $post ) {
	$address             = get_post_meta( $post->ID, '_nbm_address', true );
	$coords              = get_post_meta( $post->ID, '_nbm_coords', true );
	$coords_sexa         = get_post_meta( $post->ID, '_nbm_coords_sexa', true );
	$do_u_define_coords  = get_post_meta( $post->ID, '_nbm_do_u_define_coords', true );

	wp_enqueue_style( 'leaflet' );
	wp_enqueue_script( 'leaflet' );
	wp_enqueue_script( 'find-coords' );

	$currentCoords = $coords ? array( 'ok' => true, 'lat' => $coords['lat'], 'long' => $coords['long'] ) : array( 'ok' => false, 'lat' => 0, 'long' => 0 );
	wp_localize_script( 'find-coords', 'currentCoords', $currentCoords );

	wp_nonce_field( 'nbm_coords-save_'.$post->ID, 'nbm_coords-nonce' );

	//RETRIEVE ADDRESS & COORDS
	?>

	<textarea name="nbm_address" style="width:250px;"><?php echo esc_html( $address ); ?></textarea>
	<div id="place-pointer" class-"place-pointer" style="height:250px;width:250px;"></div>

	<div id="coord_decimal">
		<input type="text" name="nbm_coords" style="width:250px;" value="<?php echo ( ( $coords ) ? $coords['lat'] : '' ) . ' , ' . ( ( $coords ) ? $coords['long'] : '' ); ?>" disabled="disabled" id="gps_coords" /><br/>
	</div>
	<div id="coord_sexa" style="display:none;" >
		<input type="text" name="n_degres" size="3" value="<?php echo  ( ( $coords_sexa ) ? (int)$coords_sexa['n_degres'] : '' ) ?>"/>
		<input type="text" name="n_minutes" size="3" value="<?php echo  ( ( $coords_sexa ) ? (int)$coords_sexa['n_minutes'] : '' ) ?>"/>
		<input type="text" name="n_secondes" size="3" value="<?php echo  ( ( $coords_sexa ) ? (int)$coords_sexa['n_secondes'] : '' ) ?>"/>
		N <br />
		<input type="text" name="w_degres" size="3" value="<?php echo  ( ( $coords_sexa ) ? (int)$coords_sexa['w_degres'] : '' ) ?>"/>
		<input type="text" name="w_minutes" size="3" value="<?php echo  ( ( $coords_sexa ) ? (int)$coords_sexa['w_minutes'] : '' ) ?>"/>
		<input type="text" name="w_secondes" size="3" value="<?php echo  ( ( $coords_sexa ) ? (int)$coords_sexa['w_secondes'] : '' ) ?>"/>
		W <br />
	</div>

	<input type="checkbox" name="nbm_do_u_define_coords" <?php checked( $do_u_define_coords, 1 ) ?> value="1" id="nbm_do_u_define_coords">
	<label for="nbm_do_u_define_coords"><?php _e( 'Do you want to manually define GPS coords of the place ?', 'nbm' ); ?></label><br/>

	<div id="choix_type_coord" style="display:none">
		<input type="radio" name="type_coord" id="choix_coord_decimal" checked /><label for="choix_coord_decimal"><strong> <?php _e( 'Decimal coordinates', 'nbm' ); ?></strong></label>
		<br />
		<input type="radio" name="type_coord" id="choix_coord_sexagesimal" /><label for="choix_coord_sexagesimal"><strong> <?php _e( 'Sexagesimal coordinates', 'nbm' ); ?></strong>
		</label>
	</div>

	<script type="text/javascript">
		//SWITCH BETWEEN MANUAL & AUTO + SEXAGESIMAL & DECIMAL
		jQuery(document).ready(function($){
			var $gps_man = $( '#nbm_do_u_define_coords' );
			var $deci    = $( '#choix_coord_decimal' );
			var $sexa    = $( '#choix_coord_sexagesimal' );

			function test_manual_coords(){
				if( $gps_man.prop( "checked" ) == true ){
					$( '#gps_coords' ).prop( "disabled", false );
					$( '#choix_type_coord' ).show();
				}else{
					$( '#gps_coords' ).prop( "disabled", true );
					$( '#choix_type_coord' ).hide();
				}
			}
			$gps_man.on( 'click', test_manual_coords );
			test_manual_coords();

			function show_deci(){
				$( '#coord_sexa' ).hide();
				$( '#coord_decimal' ).show();
			}
			$deci.on( 'click', show_deci );

			function show_sexa(){
				$( '#coord_decimal' ).hide();
				$( '#coord_sexa' ).show();
			}
			$sexa.on( 'click', show_sexa );
		});
	</script>
	<?php
}

function nbm_loc_icon( $post ) {
	wp_enqueue_style( 'nbm-admin-ui-css' );
	wp_nonce_field( 'nbm_icon-save_'.$post->ID, 'nbm_icon-nonce' );
	$data = get_post_meta( $post->ID, '_nbm_icon', true );
	$data['color'] = isset( $data['color'] ) ? $data['color'] : 'blue';
	$data['letter'] = isset( $data['letter'] ) ? $data['letter'] : 'A'; // FLAG in icon mode or A in letter mode
	$size = get_post_meta( $post->ID, '_nbm_size', true );
	$size = $size ? $size : 'medium';
	$sizes = array( 'large'=>'blue-large.png','medium'=>'blue-medium.png','small'=>'blue.png','pin'=>'blue-pin.png' );
	$type = get_post_meta( $post->ID, '_nbm_icon_type', true );
	$type = $type ? $type : 'icon';
	$data['letter'] = $type=='icon' ? $data['letter'] : 'A';
	$pin = str_replace( 'blue', $data['color'], $sizes[$size] );
	?>
		<div id="mappreview"><span class="<?php echo $type; ?> <?php echo $size; ?> "><?php echo $data['letter']; ?></span><img src="<?php echo NBM_PLUGIN_URL; ?>/css/img/pins/<?php echo $pin; ?>"></div>
	<?php
	//sizes
	echo '<h4>' . __( 'Marker size', 'nbm' ) . ' :</h4>';
	foreach ($sizes as $key => $file) { ?>
		<span class="nbm-size"><input id="nbm-size-<?php echo $key; ?>" type="radio" name="nbm-size" value="<?php echo $key; ?>" <?php checked( $size, $key ); ?>> <label for="nbm-size-<?php echo $key; ?>" ><img src="<?php echo NBM_PLUGIN_URL . '/css/img/pins/' . $file; ?>" /></label></span>&nbsp;
	<?php }

	echo '<hr />';

	//colors
	echo '<h4>' . __( 'Marker color', 'nbm' ) . ' :</h4>';
	$colors = nbm_return_icon_data( 'colors' );
	foreach( $colors as $k => $c )
		echo '<span class="nbm-color"><input id="' . $k . '" type="radio" name="nbm-colors" value="' . $k . '" '. ( ( isset( $data[ 'color'] ) ) ? checked( $data[ 'color'], $k, false ) : '' ) . '><label for="' . $k . '"><img src="' . NBM_PLUGIN_URL . '/css/img/pins/' . $k . '.png" /></label></span> ';

	echo '<hr />';

	//icons
	echo '<h4>' . __( 'Marker icon', 'nbm' ) . ' :</h4>';
	echo '<b>'.__( 'Icon type:', 'nbm' ).'</b> <label><input type="radio" name="nbm-icon-type" id="for_icon_type" value="icon" '.checked( $type, 'icon', false ).'/> '.__( 'Icon', 'nbm' ).'</label> <label><input id="for_letter_type" type="radio" name="nbm-icon-type" value="letter" '.checked( $type, 'letter', false ).'/> '.__( 'Letter', 'nbm' ).'</label>';
	echo '<div id="for_types">';
	for( $i='a';$i!='aa';$i++)
		echo '<span class="nbm-letter"><input id="f_'.$i.'" type="radio" name="nbm-letters" value="' . $i . '" '. ( ( isset( $data[ 'letter'] ) ) ? checked( $data[ 'letter'], $i, false ) : '' ) . '><label for="f_'.$i.'">' . $i . '</label></span>';
	for( $i='A';$i!='AA';$i++)
		echo '<span class="nbm-letter"><input id="s_'.$i.'" type="radio" name="nbm-letters" value="' . $i . '" '. ( ( isset( $data[ 'letter'] ) ) ? checked( $data[ 'letter'], $i, false ) : '' ) . '><label for="s_'.$i.'">' . $i . '</label></span>';
	for( $i=0;$i<=9;$i++)
		echo '<span class="nbm-letter"><input id="t_'.$i.'" type="radio" name="nbm-letters" value="' . $i . '" '. ( ( isset( $data[ 'letter'] ) ) ? checked( $data[ 'letter'], $i, false ) : '' ) . '><label for="t_'.$i.'">' . $i . '</label></span>';

	foreach( array('²','~','#','{','[','|','`','\\','ç','^','°',')',']','+','=','}','¨','£','$','¤','%','ù','µ','*','?',',','.',';','/','§','!','<','>','-','¡','¢','¥','¦','©','ª','±','¯','®','¬','«','³','´','¶','·','¸','¹','º','»','¼','½','¾','¿','À','Á','Â','Ã','Ä','Å','Æ','Ç','È','É','Ê','Ë','Ì','Í','Î','Ï','Ð','Ñ','Ò','Ó','Ô','Õ','Ö','Ø','Ù','Ú','Û','Ü','Ý','æ') as $i )
		echo '<span class="nbm-letter"><input id="q_'.$i.'" type="radio" name="nbm-letters" value="' . $i . '" '. ( ( isset( $data[ 'letter'] ) ) ? checked( $data[ 'letter'], $i, false ) : '' ) . '><label for="q_'.$i.'">' . $i . '</label></span>';
	echo '</div>';
	echo '<div id="for_letters" class="hidden">';
	_e( '<p>Letters will automatically be called from A, B, C to Z, then AA, AB, AC to ZZ etc<p>', 'nbm' );
	echo '</div>';
	?>
	<script>
	jQuery( document ).ready( function($){
		$('#for_icon_type').on('click', function(){ $('#for_types').show(); $('#for_letters').hide(); } );
		$('#for_letter_type').on('click', function(){$('#for_types').hide(); $('#for_letters').show(); } );
		$('#for_letter_type:checked').click();
		$('.nbm-size, .nbm-color, .nbm-letter, #for_letter_type, #for_icon_type').on('click', function(){
			var $size = $('.nbm-size input:checked').val();
			var $color = $('.nbm-color input:checked').val();
			var $letter = $('.nbm-letter input:checked').val();
			var $type = $('#for_icon_type').is(':checked') ? 'icon' : 'letter';
			var $NBM_PLUGIN_URL = '<?php echo NBM_PLUGIN_URL; ?>';
			$('#mappreview span').attr( 'class', ''+$type+' '+$size ).text( $letter );
			$size = $size=='small' ? '' : '-'+$size;
			$('#mappreview img').attr( 'src', $NBM_PLUGIN_URL + '/css/img/pins/' + $color + $size + '.png' );
			if( $type=='icon' )
				$('#mappreview span').text( $letter );
			else
				$('#mappreview span').text( 'A' );
		})
	} );
	</script>
	<?php
}

function nbm_infos($post) {
	$tel = get_post_meta( $post->ID, '_nbm_tel', true );
	$email = get_post_meta( $post->ID, '_nbm_email', true );
	$website = get_post_meta( $post->ID, '_nbm_website', true );
	$tooltip = get_post_meta( $post->ID, '_nbm_tooltip', true );
	$hours = get_post_meta( $post->ID, '_nbm_hours', true );
	$type_of_place = get_post_meta( $post->ID, '_nbm_type_of_place', true );
	$important = get_option( 'nbm_important' );

	wp_nonce_field( 'nbm_infos-save_' . $post->ID, 'nbm_infos-nonce' );
	echo '<table>';
	echo '<tr><th><label for="nbm_tel">' . __( 'Tel.:', 'nbm' ) . '</label></th><td><input type="tel" name="nbm_tel" id="nbm_tel" value="' . esc_attr( $tel ) . '"></td></tr>';
	echo '<tr><th><label for="nbm_email">' . __( 'E-mail:', 'nbm' ) . '</label></th><td><input type="email" name="nbm_email" id="nbm_email" value="' . esc_attr( $email ) . '"></td></tr>';
	echo '<tr><th><label for="nbm_website">' . __( 'Website:', 'nbm' ) . '</label></th><td><input type="url" name="nbm_website" id="nbm_website" value="' . esc_attr( $website ) . '"></td></tr>';
	echo '<tr><td colspan="2"><label for="nbm_tooltip">' . __( 'Tooltip:', 'nbm' ) . '</label> <textarea name="nbm_tooltip" id="nbm_tooltip" style="width:100%;">' . esc_textarea( $tooltip ) . '</textarea></td></tr>';
	echo '<tr><th><label for="nbm_type_of_place">' . __( 'Type of place:', 'nbm' ) . '</label></th><td><select id="nbm_type_of_place" name="nbm_type_of_place">';
		echo '<option value="Place">' . __( 'just a place...', 'nbm' ) . '</option>';
		echo '<option ' . selected( $type_of_place, 'AdministrativeArea', false ) . ' value="AdministrativeArea">' . __( 'administrative area', 'nbm' ) . '</option>';
		echo '<option ' . selected( $type_of_place, 'CivicStructure', false ) . ' value="CivicStructure">' . __( 'civic structure', 'nbm' ) . '</option>';
		echo '<option ' . selected( $type_of_place, 'Landform', false ) . ' value="Landform">' . __( 'landform', 'nbm' ) . '</option>';
		echo '<option ' . selected( $type_of_place, 'LandmarksOrHistoricalBuildings', false ) . ' value="LandmarksOrHistoricalBuildings">' . __( 'historical landmarks/building', 'nbm' ) . '</option>';
		echo '<option ' . selected( $type_of_place, 'LocalBusiness', false ) . ' value="LocalBusiness">' . __( 'local business', 'nbm' ) . '</option>';
		echo '<option ' . selected( $type_of_place, 'Residence', false ) . ' value="Residence">' . __( 'residence', 'nbm' ) . '</option>';
		echo '<option ' . selected( $type_of_place, 'TouristAttraction', false ) . ' value="TouristAttraction">' . __( 'tourist attraction', 'nbm' ) . '</option>';
	echo '</select></td></tr>';
	echo '<tr><td colspan="2"><label for="nbm_hours">' . __( 'Opening hours:', 'nbm' ) . '</label><br/><textarea name="nbm_hours" id="nbm_hours" style="width:100%;">' . esc_textarea( $hours ) . '</textarea></td></tr>';
	// echo '<hr>';
	echo '<tr><td colspan="2"><label><b>' . __( 'Is there the central place?', 'nbm' ) . '</b></label><br/><input type="radio" name="nbm_important" value="yes" id="is_important" ' . checked( $important, $post->ID, false ) . '> <label for="is_important">' . __( 'Yes it is', 'nbm' ) . '</label><br/><input type="radio" name="nbm_important" value="no" id="is_no_important"' . ( ( $important != $post->ID ) ? ' checked="checked"' : '' ) . '> <label for="is_no_important">' . __( 'No it isn\'t', 'nbm' ) . '</label></td></tr>';
	echo '</table>';
}

add_action( 'load-edit.php', 'nbm_add_columns' );
function nbm_add_columns()
{
	add_action( 'manage_'.apply_filters( 'nbm_post_type', 'places' ).'_posts_columns', 'nbm_add_address_column', 10, 2 );
	function nbm_add_address_column( $columns )
	{
		$columns['nbmaddress'] = __( 'Address', 'nbm' );
		return $columns;
	}

	add_action( 'manage_'.apply_filters( 'nbm_post_type', 'places' ).'_posts_custom_column', 'nbm_render_address_columns', 10, 2 );
	function nbm_render_address_columns( $column_name, $id )
	{
		if ( $column_name == 'nbmaddress' ){
			$adresse = get_post_meta( $id, '_nbm_address', true );
			echo str_replace( "\n", '<br />', $adresse );
		}
	}
}

add_filter( 'plugin_action_links_' . plugin_basename( NBM__FILE__ ), 'nbm_settings_action_links' );
function nbm_settings_action_links( $links )
{
	array_unshift( $links, '<a href="' . admin_url( 'options-general.php?page=nbm_settings' ) . '">' . __( 'Settings' ) . '</a>' );
	return $links;
}

add_action( 'admin_menu', 'nbm_create_menu' );
function nbm_create_menu()
{
	add_options_page( NBM_FULLNAME, NBM_FULLNAME, 'manage_options', 'nbm_settings', 'nbm_settings_page' );
	register_setting( 'nbm_settings', 'nbm' );
}

register_activation_hook( NBM__FILE__, 'nbm_activation' );
function nbm_activation()
{
	add_option( 'nbm', array( 'apikey'=>'8EE2A50541944FB9BCEDDED5165F09D9', 'service'=>'cloudmade', 'style'=>997 ) );
}

register_uninstall_hook( NBM__FILE__, 'nbm_uninstaller' );
function nbm_uninstaller()
{
	delete_option( 'nbm' );
}

function nbm_settings_page()
{
	add_settings_section( 'nbm_settings_page', __( 'General', 'nbm' ), '__return_false', 'nbm_settings' );
		add_settings_field( 'nbm_field_api_key', __( 'Cloudmade API Key', 'nbm' ), 'nbm_field_text', 'nbm_settings', 'nbm_settings_page', array( 'name'=>'apikey', 'label'=>__( 'Cloudmade API Key', 'nbm' ), 'description'=>__( 'You can get a free or premium account here: ', 'nbm' ).'<a href="http://account.cloudmade.com/register" target="_blank">Cloudmade Register</a>' ) );
		add_settings_field( 'nbm_field_style', __( 'Cloudmade Style', 'nbm' ), 'nbm_field_style', 'nbm_settings', 'nbm_settings_page', array( 'name'=>'style', 'label'=>__( 'Cloudmade Style', 'nbm' ), 'description'=>__( 'Enter a tile theme (only the ID) from here:', 'nbm' ).'<a href="http://maps.cloudmade.com/editor" target="_blank">Cloudmade Map Editor</a>' ) );
		add_settings_field( 'nbm_field_map_service', __( 'Which full map service do you want?', 'nbm' ), 'nbm_field_map_service', 'nbm_settings', 'nbm_settings_page' );
?>
	<div class="wrap">
	<div id="icon-nbm" class="icon32" style="background: url(<?php echo NBM_PLUGIN_URL; ?>/css/img/icon32.png) 0 0 no-repeat;"><br/></div>
	<h2><?php echo NBM_FULLNAME; ?> <small>v<?php echo NBM_VERSION; ?></small></h2>

	<form action="options.php" method="post">
		<?php settings_fields( 'nbm_settings' ); ?>
		<div class="tabs"><?php do_settings_sections( 'nbm_settings' ); ?></div>
		<div class="tabs"><?php do_settings_sections( 'nbm_settings2' ); ?></div>
		<?php submit_button(); ?>
	</form>
<?php
}

function nbm_field_map_service()
{
	$nbm_options = get_option( 'nbm' );
	?>
	<fieldset>
		<legend class="screen-reader-text"><span><?php _e( 'Which full map service do you want?', 'nbm' ); ?></span></legend>
		<label><input type="radio" name="nbm[service]" value="cloudmade" <?php checked( $nbm_options['service'], 'cloudmade' ); ?>/> Cloudmade</label>
		<br />
		<label><input type="radio" name="nbm[service]" value="google" <?php checked( $nbm_options['service'], 'google' ); ?> /> Google</label>
	</fieldset>
<?php }

function nbm_field_text( $attr )
{
	$nbm_options = get_option( 'nbm' );
	?>
	<fieldset>
		<legend class="screen-reader-text"><span><?php echo $attr['label']; ?></span></legend>
		<label><input type="text" name="nbm[<?php echo $attr['name']; ?>]" class="regular-text" value="<?php echo $nbm_options[$attr['name']]; ?>" /></label><br />
		<em><?php echo $attr['description']; ?></em>
	</fieldset>
<?php }

function nbm_field_style()
{
	wp_enqueue_style( 'nbm-options' );
	$nbm_options = get_option( 'nbm' );
	// delete_transient( 'nbm-defaut-styles' );
	?>
	<fieldset>
	<?php
	$group = (int)apply_filters( 'nbm_option_group_tiles', isset( $_GET['group'] ) ? $_GET['group'] : 3 );
	if( !$resp = get_transient( 'nbm-defaut-styles-'.$group ) ){
		$resp = wp_remote_get( 'http://maps.cloudmade.com/map_styles/gallery_search?group='.$group.'&color=-14&text=' );
		if( !is_wp_error( $resp ) )
			set_transient( 'nbm-defaut-styles-'.$group, $resp, DAY_IN_SECONDS );
	}
	if( !is_wp_error( $resp ) ){
		$json = (array)json_decode( wp_remote_retrieve_body( $resp ) );
		?>
			<legend class="screen-reader-text"><span><?php _e( 'Which full map service do you want?', 'nbm' ); ?></span></legend>
		<?php
		if( !is_wp_error( $resp ) ){
			$i = 0;
			foreach( $json as $item ){
				$i++;
				$item = (array)$item;
				$checked = checked( $nbm_options['style'], $item['id'], false );
				$selected = $checked!='' ? ' selected' : '';
				?>
				<label>
					<input type="radio" class="hidden nbmstyle" name="nbm[style]" value="<?php echo $item['id']; ?>"<?php echo $checked; ?>/>
					<div class="nbmpreview">
						<div class="preview">
							<img alt="<?php echo $item['name']; ?>" src="http://cloudmade.com.s3.amazonaws.com/se/styles/<?php echo $item['id']; ?>/preview.gif" />
						</div>
						<div class="name-wrapper">
							<h4><?php echo $item['name']; ?></h4>
						</div>
					</div>
				</label>
				<?php
				echo $i % 4 == 0 ? '<div class="clear"></div>' : '';
			}
		}
	}
	?>
	</fieldset>
	<?php if( !isset( $_GET['group'] ) || $_GET['group']=='3' ){ ?>
		<a href="<?php echo admin_url( 'options-general.php?page=nbm_settings&group=0' ); ?>"><?php _e( 'Load all styles', 'nbm' ); ?></a>
	<?php }else{ ?>
		<a href="<?php echo admin_url( 'options-general.php?page=nbm_settings&group=3' ); ?>"><?php _e( 'Load only default styles', 'nbm' ); ?></a>
	<?php }
}