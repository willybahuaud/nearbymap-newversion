jQuery( document ).ready( function($) {
    if( document.getElementById( 'place-pointer' ) ) {
        var ppointer = L.map( 'place-pointer' ).setView( [ currentCoords['lat'], currentCoords['long'] ], 13);
        L.tileLayer( 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo( ppointer );
        var fmarker = L.marker([ currentCoords['lat'], currentCoords['long'] ], {
            draggable:true
        } ).addTo( ppointer );

        fmarker.on('dragend', function(event){
            var marker = event.target;
            var position = marker.getLatLng();
            ppointer.setView( position, ppointer.getZoom() );
            updateCoords( position );
        } );
    }

    function updateCoords( coords ) {
        var $dec = $( '#gps_coords' );
        var $sex = $( '#coord_sexa' );

        $dec.val( coords.lat.toFixed(7) + ', ' + coords.lng.toFixed(7) );
        var sexaC = [decToSex(coords.lat),decToSex(-coords.lng)];
        $sex.find( '[name="n_degres"]' ).val( sexaC[0][0] );
        $sex.find( '[name="n_minutes"]' ).val( sexaC[0][1] );
        $sex.find( '[name="n_secondes"]' ).val( sexaC[0][2] );
        $sex.find( '[name="w_degres"]' ).val( sexaC[1][0] );
        $sex.find( '[name="w_minutes"]' ).val( sexaC[1][1] );
        $sex.find( '[name="w_secondes"]' ).val( sexaC[1][2] );
    }

    function decToSex( dec ) {
        var d = parseInt( dec );
        var ms = ( dec - d ) * 60;
        var m = parseInt( ms );
        var s = ( ms - m ) * 60;
        return [d,m,s];
    }
} );