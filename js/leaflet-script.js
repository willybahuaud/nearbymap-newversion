/* getElementsByClassName -> http://stackoverflow.com/questions/9427311/how-to-get-all-elements-by-class-name*/
if (!document.getElementsByClassName) {
    document.getElementsByClassName=function getElementsByClass(searchClass,node,tag) {
	    var classElements = new Array();
	    if ( node == null )
	        node = document;
	    if ( tag == null )
	        tag = '*';
	    var els = node.getElementsByTagName(tag);
	    var elsLen = els.length;
	    var pattern = new RegExp("(^|\\s)"+searchClass+"(\\s|$)");
	    for (i = 0, j = 0; i < elsLen; i++) {
	        if ( pattern.test(els[i].className) ) {
	            classElements[j] = els[i];
	            j++;
	        }
	    }
	    return classElements;
	}
}

var last_route = '';

//for each mapID, do stuff...
var allMaps = document.getElementsByClassName('nbm-map');
for(var i=0;i<allMaps.length;i++){
	var idcurrent = allMaps[i].getAttribute('id');
	build_a_map(idcurrent);
}

function build_a_map(mapid) {
	var data = window['maps_datas_' + mapid];
	var map = L.map( mapid ).setView([data[ 'markers' ][0]['coords']['lat'], data[ 'markers' ][0]['coords']['long']], 13);
	var type = data[ 'type' ]=='icon' ? '' : ' nbm-letter';
	L.tileLayer( data[ 'tiles' ], {
		maxZoom: data[ 'max-zoom' ],
		attribution: data[ 'attribution' ],
		subdomains: data[ 'subdomains' ]
	}).addTo(map);

	var smallIcon = L.DivIcon.extend({
	    options: {
			iconSize:     data['iconProp']['iconSizeSmall'],
			iconAnchor:   data['iconProp']['iconAnchorSmall'],
			className:    data['iconProp']['className']+type,
			popupAnchor:  data['iconProp']['popupAnchorSmall']
	    }
	});
	var mediumIcon = L.DivIcon.extend({
	    options: {
			iconSize:     data['iconProp']['iconSizeMedium'],
			iconAnchor:   data['iconProp']['iconAnchorMedium'],
			className:    data['iconProp']['className']+type,
			popupAnchor:  data['iconProp']['popupAnchorMedium']
	    }
	});
	var pinIcon = L.DivIcon.extend({
	    options: {
			iconSize:     data['iconProp']['iconSizePin'],
			iconAnchor:   data['iconProp']['iconAnchorPin'],
			className:    data['iconProp']['className']+type,
			popupAnchor:  data['iconProp']['popupAnchorPin']
	    }
	});
	var largeIcon = L.DivIcon.extend({
	    options: {
			iconSize:     data['iconProp']['iconSizeLarge'],
			iconAnchor:   data['iconProp']['iconAnchorLarge'],
			className:    data['iconProp']['className']+type,
			popupAnchor:  data['iconProp']['popupAnchorLarge']
	    }
	});

	//drops markers to map
	bound = new Array;
	for( var i = 0; i < data[ 'markers' ].length; i++ ) {
		var m = data[ 'markers' ][ i ];
		var markerHtml = '<div class="icon type-' + m['type'] + '" data-marker="' + m['id'] + '">' + m['icon']['letter'] + '</div>';
		if( m['import'] )
			markerHtml += '<div class="central-point"></div>';
		markerHtml += '<div class="shadow"></div>';
		switch(m['size']){
			case 'large' :
				var iconMarker = new largeIcon({className:'nbm-icon ' + m['icon']['color'] + ' ' + m['size'], html: markerHtml});
				break;
			case 'medium' :
				var iconMarker = new mediumIcon({className:'nbm-icon ' + m['icon']['color'] + ' ' + m['size'], html: markerHtml});
				break;
			case 'pin' :
				var iconMarker = new pinIcon({className:'nbm-icon ' + m['icon']['color'] + ' ' + m['size'], html: markerHtml});
				break;
			default :
				var iconMarker = new smallIcon({className:'nbm-icon ' + m['icon']['color'] + ' ' + m['size'], html: markerHtml});
		}

		L.marker([m['coords']['lat'], m['coords']['long']], { 'icon' : iconMarker } ).addTo(map)
			.bindPopup(m[ 'datas' ]);
			bound.push([m['coords']['lat'], m['coords']['long']]);
	}
	var zoom = (map.getBoundsZoom(bound)-1 < data[ 'max-zoom' ]) ? map.getBoundsZoom(bound)-1 : data[ 'max-zoom' ];
	map.setView(L.latLngBounds(bound).getCenter(), zoom);


	jQuery(document).ready(function($){
		//from map to info
		var $nbm = $('.nbm-all-places[data-map="' + mapid + '"]');
		var $map = $('#' + mapid);
		$map.find('.icon').hover(function(){
			var data = $(this).attr('data-marker');
			$nbm.find($('[data-nbm="' + data + '"]')).addClass('hover');
		},function(){
			var data = $(this).attr('data-marker');
			$nbm.find($('[data-nbm="' + data + '"]')).removeClass('hover');
		});

		//from infos to map
		$nbm.find('.nbm-more').hover(function(){
			var data = $(this).attr('data-nbm');
			$map.find($('[data-marker="' + data + '"]')).addClass('hover');
		},function(){
			var data = $(this).attr('data-nbm');
			$map.find($('[data-marker="' + data + '"]')).removeClass('hover');
		});


		if(document.getElementsByClassName('nbm-route-form')) {
			var notRoutedAgain = true;
			var firstpolyline = '';

			// show custom point
			$('.nbm-route-form[data-map="' + mapid + '"]').on('change', '.nbm-select',function(){
				if($(this).val() == 'custom'){
					$(this).parent().find('input').show();
				}else{
					$(this).parent().find('input').hide();
				}
			});

			// sShow options
			$('a[data-map="' + mapid + '"]').on('click',function(e){
				e.preventDefault();
				$('.nbm-route-options[data-map="' + mapid + '"]').slideToggle();
			});


	/**
	 ROUTE
	*/
			$('.nbm-route-form[data-map="' + mapid + '"]').on('submit',function(e){
				e.preventDefault();
				var $t = this;
				if( $($t).find('select[name="nbm-start"]').val()=='' || $($t).find('select[name="nbm-end"]').val()=='' )
					return false;
				var routeQuery = $($t).serialize();
				if( last_route == routeQuery ){
					$($t).find('.route-details').slideToggle();
					return false;
				}
				$($t).find('.nbm-route-options').slideUp('slow');
				last_route = routeQuery;
				var routeDetails = '';

				function printArrows(arr){
					switch(arr){
						case 'TR':
							return '<span>&#8625;</span> ';
							break;
						case 'TL':
							return '<span>&#8624;</span> ';
							break;
						case 'C':
							return '<span>&uarr;</span> ';
							break;
						case 'EXIT1':
							return '<span>&#8634;</span> ';
							break;
						case 'EXIT2':
							return '<span>&#8634;</span> ';
							break;
						case 'EXIT3':
							return '<span>&#8634;</span> ';
							break;
						case 'EXIT4':
							return '<span>&#8634;</span> ';
							break;
						case 'EXIT5':
							return '<span>&#8634;</span> ';
							break;
						case 'EXIT6':
							return '<span>&#8634;</span> ';
							break;
						case 'EXIT7':
							return '<span>&#8634;</span> ';
							break;
						case 'EXIT8':
							return '<span>&#8634;</span> ';
							break;
						case 'TSLR':
							return '<span>&#8625;</span> ';
							break;
						case 'TSLL':
							return '<span>&#8624;</span> ';
							break;
						default:
							return '';
					}
				}

				function wichTransp(trs){
					if( trs != 'bicycle' && trs != 'foot' )
						return 'car';
					return trs;
				}

				function wichSpeed(trs){
					switch(trs){
						case 'bicycle':
							return 1000;
							break;
						case 'foot':
							return 1250;
							break;
						default :
							return 500;
							break;
					}
				}



				$($t).find('.nbm_load').show();
				$($t).find('.route-details').slideUp('slow');

				//ROUTE
				$.ajax({
					url: data['admin_url'],
					type:'POST',
					data:'action=nbm_get_route&' + routeQuery,
					complete: function(){
						$($t).find('.nbm_load').hide();
					},
					success : function(result){
						$($t).find('.route-details').slideDown('slow');
						var obj = $.parseJSON(result);

						//PATH
						var path = new Array;
						for(i in obj.route_geometry) {
							path.push( new L.LatLng(obj.route_geometry[i][0],obj.route_geometry[i][1]));
						}

						if(notRoutedAgain == true) {
							firstpolyline = new L.Polyline(path, {
								color: '#4fdaea',
								weight: 5,
								opacity: 0.5,
								smoothFactor: 1
							});
							firstpolyline.addTo(map);
							notRoutedAgain = false;
						}else{
							firstpolyline.setLatLngs(path).redraw();
						}

						map.fitBounds(firstpolyline.getBounds());

						//INFORMATION
						var dial = window['routeDial'];
						var time = obj.route_summary['total_time'];
						var minutes = ~~(time / 60);
						var hours = ~~(minutes / 60);
						var days = ~~(hours / 24);
						if( days > 0 ){
							hours = hours % 24;
							days = ''+days+dial['days'];
						}else days = '';
						if( hours > 0 ){
							minutes = minutes % 60;
							hours = ''+hours+dial['hours'];
						}else hours = '';
						if( minutes > 0 )
							minutes = ''+minutes+dial['minutes'];
						var time = days+' '+hours+' '+minutes;
						routeDetails = '<div class="nbm-route-title">' + obj.route_summary['start_point'] + ' ' + dial['to'] + ' ' + obj.route_summary['end_point'] + ' (' + parseInt(obj.route_summary['total_distance']/1000) + 'km ' + dial['in'] + ' ' + time + ')</div>';
						routeDetails += '<ol id="nbm-route-details" class="nbm-route-details">';
						for(i in obj.route_instructions) {
							routeDetails += '<li class="route-detail"><div class="nbm-route-instr">' + printArrows(obj.route_instructions[i][7]) + obj.route_instructions[i][0] + '<div><div class="nbm-route-dist">' + obj.route_instructions[i][4] + '<div></li>';
						}
						routeDetails += '</ol>';
						$('.route-details[data-map="' + mapid + '"]').html(routeDetails);


						//TRIP
						var bikeIcon = L.icon({
							iconUrl: dial['transIcon'][wichTransp($('.nbm-trans:checked').val())],
							iconSize: [25, 39],
							iconAnchor: [12, 39],
							shadowUrl: null
						});
						var velo = L.animatedMarker(firstpolyline.getLatLngs(), {
							icon: bikeIcon,
							autoStart: false,
							interval: wichSpeed($('.nbm-trans:checked').val()),
							onEnd: function() {
								$(this._shadow).fadeOut();
								$(this._icon).fadeOut(3000, function(){
									map.removeLayer(this);
								});
							}
						});
						map.addLayer(velo);

						$(velo._icon).hide().fadeIn(1000, function(){
							velo.start();
						});
					}
				});
			});
		}
	});
}