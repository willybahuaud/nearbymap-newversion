<?php
/*
Plugin Name: Nearby Map by Wabeo
Plugin URI: http://nearbymap.wabeo.fr
Description: Allow to build a map to show the activities, places and services around a given geographical point.
Version: 1
Author: Willy Bahuaud
Author URI: http://wabeo.fr
Contributor: juliobox
License: GPLv2
TextDomain: nbm
DomainPath: /lang
*/

define( 'NBM_PLUGIN_URL', trailingslashit( WP_PLUGIN_URL ) . basename( dirname( __FILE__ ) ) );
define( 'NBM__FILE__', __FILE__ );
define( 'NBM_FULLNAME', 'Nearby Map' );
define( 'NBM_VERSION', '0.9.8' );

// Plugin Bootstrap
add_action( 'plugins_loaded', 'nbm_bootstrap' );
function nbm_bootstrap()
{
	$filename  = 'inc/';
	$filename .= is_admin() ? 'backend' : 'frontend';
	if( file_exists( plugin_dir_path( __FILE__ ) . $filename . '-both-ajax.inc.php' ) )
		include( plugin_dir_path( __FILE__ ) . $filename . '-both-ajax.inc.php' );
	$filename .= defined( 'DOING_AJAX' ) && DOING_AJAX ? '' : '-no';
	$filename .= '-ajax.inc.php';
	if( file_exists( plugin_dir_path( __FILE__ ) . $filename ) )
		include( plugin_dir_path( __FILE__ ) . $filename );
	$filename  = 'inc/';
	$filename .= 'bothend';
	if( file_exists( plugin_dir_path( __FILE__ ) . $filename . '-both-ajax.inc.php' ) )
		include( plugin_dir_path( __FILE__ ) . $filename . '-both-ajax.inc.php' );
	$filename .= defined( 'DOING_AJAX' ) && DOING_AJAX ? '' : '-no';
	$filename .= '-ajax.inc.php';
	if( file_exists( plugin_dir_path( __FILE__ ) . $filename ) )
		include( plugin_dir_path( __FILE__ ) . $filename );
	if( file_exists( plugin_dir_path( __FILE__ ) . 'inc/always.inc.php' ) )
		include( plugin_dir_path( __FILE__ ) . 'inc/always.inc.php' );
}